from django.utils import timezone
from django.forms import SelectDateWidget
from django import forms
from .models import Book


# def past_years(ago):
#     this_year = timezone.now().year
#     return list(range(this_year-ago-1,this_year))

# class DateInput(forms.DateInput):
#     input_type="date"

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = "__all__"
        widgets = {
            # "publication_date": DateInput(),
            "publication_date": forms.DateInput(attrs={
                'type':'date',
                'required':True
            })
        }

